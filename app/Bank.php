<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Bank extends Model
{
  protected $table = 'bank';
  // protected $fillable = ['nama','deskripsi'];
  public $timestamps = true;
  protected $guarded = ['created_at', 'updated_at'];
  // protected $dates = [];

  public function penjual()
  {
    return $this->hasMany('App\Bank', 'bank_id', 'id');
  }
}
