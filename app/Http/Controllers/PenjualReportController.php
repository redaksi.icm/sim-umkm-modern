<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Barang;
use App\PenjualKonven;
use App\BarangJual;
use App\DetailTransaksiKonven;
use App\TransaksiKonven;
use App\StatusHarian;
use App\Status;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use PDF;
use App\Exports\PenjualExport;
use App\Exports\PenjualHarianExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
Use Alert;
use DB;


class PenjualReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $penjual = User::where('level','Penjual')->get()->sortBy('name');
        // $penjual = Penjual::all()->sortBy('name');
        return view('penjual-report.find',compact('penjual'));
        // dd($pembeli);
    }

    public function laporan(Request $request)
    {

      date_default_timezone_set('Asia/Jakarta');

      $nama_toko = DB::table('penjual_konven')->where('user_id', $request->penjual)->pluck('nama_toko')->first(); 

      if (Carbon::parse($request->from) > Carbon::parse($request->until)) {
        return redirect()->back()->with('error', 'Data tidak cocok!');
      }
      $this->validate(request(),
        [
          'penjual' => 'required',
          'from' => 'required',
          'until' => 'required',
        ],
        [
          'penjual.required' => 'Silahkan pilih penjual!',
          'from.required' => 'Tanggal awal tidak boleh kosong!',
          'until.required' => 'Tanggal akhir tidak boleh kosong!',
        ]
      );

       $list = DetailTransaksiKonven::leftJoin('transaksi_konven', 'detail_transaksi_konven.transaksi_konven_id', '=', 'transaksi_konven.id')
                              ->leftJoin('jenis_pembayaran', 'transaksi_konven.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                              ->leftJoin('pembeli', 'transaksi_konven.pembeli_id', '=', 'pembeli.id')
                              ->leftJoin('penjual_konven', 'transaksi_konven.penjual_konven_id', '=', 'penjual_konven.id')
                              ->leftJoin('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
                              ->leftJoin('users', 'penjual_konven.user_id', '=', 'users.id')
                              ->where('barang_konven.nama','LIKE','%'.$nama_toko.'%')
                              ->where('transaksi_konven.updated_at', '>=', $request->from = Carbon::parse($request->from))
                              ->where('transaksi_konven.updated_at', '<=', $request->until = Carbon::parse($request->until)->addDay(1))
                              ->select('transaksi_konven.updated_at', 'transaksi_konven.id as transaksi_konven_id', 'barang_konven.nama as nama_barang', 'detail_transaksi_konven.harga', 'detail_transaksi_konven.diskon', 'penjual_konven.nama_toko','pembeli.nama as nama_pembeli', 'detail_transaksi_konven.jumlah', 'detail_transaksi_konven.total', 'jenis_pembayaran.jenis_pembayaran', 'transaksi_konven.status')
                              ->get();

        // $solds = DB::table('transaksi_konven')
        //                   ->join('detail_transaksi_konven','detail_transaksi_konven.transaksi_konven_id','=','transaksi_konven.id')
        //                   ->join('barang_konven','detail_transaksi_konven.barang_konven_id','=','barang_konven.id')
        //                   ->join('penjual_konven','transaksi_konven.penjual_konven_id','=','penjual_konven.id')
        //                   ->selectRaw('sum(detail_transaksi_konven.jumlah)*detail_transaksi_konven.harga as income', 'sum(detail_transaksi_konven.jumlah) as item')
        //                   ->where('barang_konven.nama','LIKE','%'.$nama_toko.'%')
        //                   ->where('status', '=', 'Lunas')
        //                   ->where('transaksi_konven.updated_at', '>=', $request->from = Carbon::parse($request->from))
        //                   ->where('transaksi_konven.updated_at', '<=', $request->until = Carbon::parse($request->until)->addDay(1))
        //                   ->groupBy('barang_konven.id','barang_konven.nama','barang_konven.stok','detail_transaksi_konven.harga','detail_transaksi_konven.jumlah','penjual_konven.nama_toko','transaksi_konven.created_at')
        //                   // ->sum('detail_transaksi_konven.total');
        //                   ->get();
                          // dd($saldo_hari_ini);

        $solds = DetailTransaksiKonven::leftJoin('transaksi_konven', 'detail_transaksi_konven.transaksi_konven_id', '=', 'transaksi_konven.id')
                              ->leftJoin('penjual_konven', 'transaksi_konven.penjual_konven_id', '=', 'penjual_konven.id')
                              ->leftJoin('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
                              ->groupBy('barang_konven.id','barang_konven.nama','barang_konven.status','detail_transaksi_konven.harga','detail_transaksi_konven.total','penjual_konven.nama_toko','transaksi_konven.created_at')
                              ->where('barang_konven.nama','LIKE','%'.$nama_toko.'%')
                              ->where('transaksi_konven.status','=','Lunas')
                              ->where('transaksi_konven.updated_at', '>=', $request->from = Carbon::parse($request->from))
                              ->where('transaksi_konven.updated_at', '<=', $request->until = Carbon::parse($request->until)->addDay(1))
                              ->selectRaw('sum(detail_transaksi_konven.jumlah)*detail_transaksi_konven.harga as income, sum(jumlah) as item')
                              ->get();
                              // dd($solds);

        $incomeTotal=0;
        $itemTotal=0;
        foreach ($solds as $key => $value) {
            $incomeTotal += $value['income'];
            $itemTotal += $value['item'];
        }

        $penjual = User::where('id',$request->penjual)->first();
        $requested = $request;
        $penjualAll = User::where('level','Penjual')->get()->sortBy('name');
        // dd($requested);
        return view('penjual-report.laporan',compact('penjual','list', 'requested', 'penjualAll', 'incomeTotal', 'itemTotal'));
      }

    public function downloadPDF(request $request){

        $nama_toko = DB::table('penjual_konven')->where('user_id', $request->penjual)->pluck('nama_toko')->first();

       $list = DetailTransaksiKonven::leftJoin('transaksi_konven', 'detail_transaksi_konven.transaksi_konven_id', '=', 'transaksi_konven.id')
                              ->leftJoin('jenis_pembayaran', 'transaksi_konven.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                              ->leftJoin('pembeli', 'transaksi_konven.pembeli_id', '=', 'pembeli.id')
                              ->leftJoin('penjual_konven', 'transaksi_konven.penjual_konven_id', '=', 'penjual_konven.id')
                              ->leftJoin('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
                              ->leftJoin('users', 'penjual_konven.user_id', '=', 'users.id')
                              ->where('barang_konven.nama','LIKE','%'.$nama_toko.'%')
                              ->where('transaksi_konven.updated_at', '>=', $request->from = Carbon::parse($request->from))
                              ->where('transaksi_konven.updated_at', '<=', $request->until = Carbon::parse($request->until)->addDay(1))
                              ->select('transaksi_konven.updated_at', 'transaksi_konven.id as transaksi_konven_id', 'barang_konven.nama as nama_barang', 'detail_transaksi_konven.harga', 'detail_transaksi_konven.diskon', 'penjual_konven.nama_toko','pembeli.nama as nama_pembeli', 'detail_transaksi_konven.jumlah', 'detail_transaksi_konven.total', 'jenis_pembayaran.jenis_pembayaran', 'transaksi_konven.status')
                              ->get();

        $solds = DetailTransaksiKonven::leftJoin('transaksi_konven', 'detail_transaksi_konven.transaksi_konven_id', '=', 'transaksi_konven.id')
                              ->leftJoin('penjual_konven', 'transaksi_konven.penjual_konven_id', '=', 'penjual_konven.id')
                              ->leftJoin('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
                              ->groupBy('barang_konven.id', 'barang_konven.id', 'barang_konven.harga')
                              ->where('penjual_konven.user_id', $request->penjual)
                              ->where('transaksi_konven.status','=','Lunas')
                              ->where('transaksi_konven.updated_at', '>=', $request->from = Carbon::parse($request->from))
                              ->where('transaksi_konven.updated_at', '<=', $request->until = Carbon::parse($request->until)->addDay(1))
                              ->selectRaw('sum(jumlah)*barang_konven.harga as income, sum(jumlah) as item')
                              ->get();

      $incomeTotal=0;
      $itemTotal=0;
      foreach ($solds as $key => $value) {
          $incomeTotal += $value['income'];
          $itemTotal += $value['item'];
      }

          $penjual = User::where('id',$request->penjual)->first();
          // return view('penjual-report.pdf',compact('penjual','list', 'request', 'incomeTotal', 'itemTotal'));
          $pdf = PDF::loadView('penjual-report.pdf', compact('penjual','list', 'request', 'incomeTotal', 'itemTotal'));
          $request->until = Carbon::parse($request->until)->addDay(-1)->format('dmY');
          $request->from = Carbon::parse($request->from)->format('dmY');
          return $pdf->download('report-'.$penjual->name.'-'.$request->from.'-'.$request->until.'.pdf');
  }

  public function downloadExcel(Request $request)
  {
    $path1 = $request->get('from');
        $path = $request->get('until');

        $nama_toko = DB::table('penjual_konven')->where('user_id', $request->penjual)->pluck('nama_toko')->first();

        if(!empty($path1) && !empty($path)){

            $data = DB::table('detail_transaksi_konven')
                ->leftJoin('transaksi_konven', 'detail_transaksi_konven.transaksi_konven_id', '=', 'transaksi_konven.id')
                ->leftJoin('jenis_pembayaran', 'transaksi_konven.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                ->leftJoin('penjual_konven', 'transaksi_konven.penjual_konven_id', '=', 'penjual_konven.id')
                ->leftJoin('pembeli', 'transaksi_konven.pembeli_id', '=', 'pembeli.id')
                ->leftJoin('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
                ->leftJoin('users', 'penjual_konven.user_id', '=', 'users.id')
                ->where('barang_konven.nama','LIKE','%'.$nama_toko.'%')
                ->select('transaksi_konven.id','barang_konven.nama','pembeli.nama','detail_transaksi_konven.harga','detail_transaksi_konven.diskon','detail_transaksi_konven.jumlah','detail_transaksi_konven.total','jenis_pembayaran.jenis_pembayaran','detail_transaksi_konven.created_at')
                ->whereBetween('transaksi_konven.updated_at',[$path1,$path])
                ->get();

            $data= json_decode( json_encode($data), true);
            Excel::create('Data Transaksi', function($excel) use($data){
            $excel->sheet('Data Transaksi', function ($sheet) use ($data) {
                $sheet->fromArray($data);
                });
            })->download("xlsx");
        }
  }
}
