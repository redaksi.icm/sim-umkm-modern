<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\JenisPembayaran;
Use Alert;
Use DB;

class JenisPembayaranController extends Controller
{
  public function __construct()
  {
      // $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = JenisPembayaran::all();
        return view('jenis-pembayaran.list', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('jenis-pembayaran.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      date_default_timezone_set('Asia/Jakarta');
      $this->validate(request(),
        [
          'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)|max:30',
          'deskripsi' => 'required',
        ],
        [
          'nama.required' => 'Jenis pembayaran harus diisi!',
          'nama.regex' => 'Jenis pembayaran tidak boleh berupa angka!',
          'nama.max' => 'Nama terlalu panjang!',
          'deskripsi.required' => 'Deskripsi harus diisi!',
        ]
      );
      JenisPembayaran::create([
            'jenis_pembayaran' => request('nama'),
            'deskripsi' => request('deskripsi')
      ]);
      
      return redirect('master/jenis-pembayaran')->with('success','Data berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = JenisPembayaran::find($id);
      return view('jenis-pembayaran.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request,$id)
     {
        date_default_timezone_set('Asia/Jakarta');
       $data= JenisPembayaran::find($id);
       $data->jenis_pembayaran=$request->nama;
       $data->deskripsi=$request->deskripsi;
      //  dd($data);
       $this->validate(request(),
       [
         'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)',
         'deskripsi' => 'required',
       ],
       [
         'nama.required' => 'Payment method can not be empty!',
         'nama.regex' => 'Payment method can not be numeric only!',
         'deskripsi.required' => 'Description can not be empty!',
       ]
       );
       $data->save();
       
       return redirect('master/jenis-pembayaran')->with('success', 'Data berhasil diperbarui!');
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

      $cek2 = DB::table('transaksi_konven')
              ->join('jenis_pembayaran', 'transaksi_konven.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
              ->where('transaksi_konven.jenis_pembayaran_id',$id)
              ->get();

      if($cek2->isNotEmpty()){
            return redirect()->back()->with('error', 'Jenis Pembayaran yang dipilih masih memiliki terdata di data transakskaksi.');
      }else{
      $data = JenisPembayaran::find($id)->delete();
      // return response()->json($data);
      }
      return redirect('master/jenis-pembayaran')->with('success', 'Data berhasil dihapus!');
    }

    public function withTrashed()
    {
      $data = JenisPembayaran::onlyTrashed()
                ->get();
      dd($data);
    }

    public function restore($id)
    {
      JenisPembayaran::withTrashed()->where('id',$id)->restore();
          return "success restore";
    }
    public function forceDelete($id)
    {
      $data = JenisPembayaran::onlyTrashed($id)->first()->forceDelete();;
      return "success force delete";
    }

// START OF API CONTROLLER

public function api_index()
    {
        $jenis = JenisPembayaran::all();
        return response()->json([
          'status'=>'success',
          'result'=>$jenis
        ]);
    }
    public function api_store(Request $request)
    {
        $pembayaran = JenisPembayaran::create([
          'jenis_pembayaran'=>$request->jenis_pembayaran,
        ]);
        // dd($request);
        return response()->json([
          'status'=>'success',
          'result'=>$pembayaran
        ]);
    }

   public function api_update(Request $request, $id)
   {
         $pembayaran= JenisPembayaran::find($id);
         $pembayaran->jenis_pembayaran=$request->jenis_pembayaran;
         $pembayaran->save();

         return response()->json([
           'status'=>'success',
           'result'=>$pembayaran
         ]);
   }
   public function api_detail($id)
   {
         $pembayaran= JenisPembayaran::find($id);

         return response()->json([
           'status'=>'success',
           'result'=>$pembayaran
         ]);
   }

   public function api_destroy($id)
   {
       $pembayaran = JenisPembayaran::find($id)->delete();
       return response()->json([
         'status'=>'data has been deleted',
       ]);
   }
}
