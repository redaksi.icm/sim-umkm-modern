<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class DashboardKonvenController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  
  public function index()
  {
  	date_default_timezone_set('Asia/Jakarta');
    $user = Auth::user()->penjual_konven()->pluck('id')->first(); 

    $produk = DB::table('barang_konven')->where('barang_konven.penjual_konven_id', '=', $user)->count();
    $pembeli = DB::table('pembeli')->count();
    $transaksi = DB::table('transaksi_konven')->where('transaksi_konven.penjual_konven_id', '=', $user)->count();
    $hari_ini = DB::table('transaksi_konven')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereBetween('created_at',[date('Y-m-d 00:00:01'),date('Y-m-d 23:59:59')])->count();

    $nama_toko = Auth::user()->penjual_konven()->pluck('nama_toko')->first(); 
    $saldo_hari_ini = DB::table('transaksi_konven')
    ->join('detail_transaksi_konven','detail_transaksi_konven.transaksi_konven_id','=','transaksi_konven.id')
    ->join('barang_konven','detail_transaksi_konven.barang_konven_id','=','barang_konven.id')
    ->join('penjual_konven','transaksi_konven.penjual_konven_id','=','penjual_konven.id')
    ->select('barang_konven.id','barang_konven.nama','barang_konven.status','detail_transaksi_konven.harga','transaksi_konven.created_at',DB::raw('sum(detail_transaksi_konven.total) as jml'), DB::raw('sum(detail_transaksi_konven.jumlah) as jmlhbarang'))
    ->where('barang_konven.nama','LIKE','%'.$nama_toko.'%')
    ->where('transaksi_konven.status', '=', 'Lunas')
    ->whereBetween('transaksi_konven.created_at',[date('Y-m-d 00:00:01'),date('Y-m-d 23:59:59')])
    ->groupBy('barang_konven.id','barang_konven.nama','barang_konven.status','detail_transaksi_konven.harga','detail_transaksi_konven.total','penjual_konven.nama_toko','transaksi_konven.created_at')
    // ->sum('detail_transaksi_konven.total');
    ->get()->sum('jml');
    // dd($saldo_hari_ini);

    $gamapay_hari_ini = DB::table('transaksi_konven')
    ->join('detail_transaksi_konven','detail_transaksi_konven.transaksi_konven_id','=','transaksi_konven.id')
    ->join('jenis_pembayaran','jenis_pembayaran.id','=','transaksi_konven.jenis_pembayaran_id')
    ->join('barang_konven','detail_transaksi_konven.barang_konven_id','=','barang_konven.id')
    ->join('penjual_konven','transaksi_konven.penjual_konven_id','=','penjual_konven.id')
    ->select('barang_konven.id','barang_konven.nama','barang_konven.status','detail_transaksi_konven.harga','transaksi_konven.created_at',DB::raw('sum(detail_transaksi_konven.total) as jml'), DB::raw('sum(detail_transaksi_konven.jumlah) as jmlhbarang'))
    ->where('barang_konven.nama','LIKE','%'.$nama_toko.'%')
    ->where('transaksi_konven.status', '=', 'Lunas')
    ->where('jenis_pembayaran', '=', 'Gamapay')
    ->whereBetween('transaksi_konven.created_at',[date('Y-m-d 00:00:01'),date('Y-m-d 23:59:59')])
    ->groupBy('barang_konven.id','barang_konven.nama','barang_konven.status','detail_transaksi_konven.harga','detail_transaksi_konven.total','penjual_konven.nama_toko','transaksi_konven.created_at')
    // ->sum('detail_transaksi_konven.total');
    ->get()->sum('jml');

    $pengeluaran_hari_ini = DB::table('transaksi_konven')
    ->join('detail_transaksi_konven','detail_transaksi_konven.transaksi_konven_id','=','transaksi_konven.id')
    ->join('jenis_pembayaran','jenis_pembayaran.id','=','transaksi_konven.jenis_pembayaran_id')
    ->join('barang_konven','detail_transaksi_konven.barang_konven_id','=','barang_konven.id')
    ->join('penjual_konven','transaksi_konven.penjual_konven_id','=','penjual_konven.id')
    ->select('barang_konven.id','barang_konven.nama','barang_konven.status','detail_transaksi_konven.harga','transaksi_konven.created_at',DB::raw('sum(detail_transaksi_konven.total_beli) as jml'), DB::raw('sum(detail_transaksi_konven.jumlah) as jmlhbarang'))
    ->where('barang_konven.nama','LIKE','%'.$nama_toko.'%')
    ->where('transaksi_konven.status', '=', 'Lunas')
    ->whereBetween('transaksi_konven.created_at',[date('Y-m-d 00:00:01'),date('Y-m-d 23:59:59')])
    ->groupBy('barang_konven.id','barang_konven.nama','barang_konven.status','detail_transaksi_konven.harga','detail_transaksi_konven.total','penjual_konven.nama_toko','transaksi_konven.created_at')
    // ->sum('detail_transaksi_konven.total');
    ->get()->sum('jml');

    $laba = $saldo_hari_ini - $pengeluaran_hari_ini;

    $stok_habis = DB::table('barang_konven')->where('barang_konven.penjual_konven_id', '=', $user)->where('status','Habis')->get();
    $populer = DB::table('transaksi_konven')
    ->join('detail_transaksi_konven','detail_transaksi_konven.transaksi_konven_id','=','transaksi_konven.id')
    ->join('barang_konven','detail_transaksi_konven.barang_konven_id','=','barang_konven.id')
    ->select('barang_konven.nama','barang_konven.nama',DB::raw('sum(detail_transaksi_konven.jumlah) as jmlhbarang'))
    ->where('barang_konven.penjual_konven_id', '=', $user)
    ->groupBy('barang_konven.id','barang_konven.nama')
    ->orderBy('jmlhbarang','desc')->limit(5)->get();

    $barang = DB::table('barang_konven')
    ->join('penjual_konven', 'barang_konven.penjual_konven_id', '=', 'penjual_konven.id')
    ->join('kategori', 'barang_konven.kategori_id', '=', 'kategori.id')
    ->select('barang_konven.*', 'kategori.kategori')
    ->where('barang_konven.penjual_konven_id', '=', $user)
    ->where('status','Habis')
    ->get();

    $data1 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','1')->count();
    $data2 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','2')->count();
    $data3 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','3')->count();
    $data4 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','4')->count();
    $data5 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','5')->count();
    $data6 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','6')->count();
    $data7 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','7')->count();
    $data8 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','8')->count();  
    $data9 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','9')->count();  
    $data10 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','10')->count();  
    $data11 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','11')->count();  
    $data12 = DB::table('transaksi_konven')->select('id')->where('transaksi_konven.penjual_konven_id', '=', $user)->whereYear('created_at',date('Y'))->whereMonth('created_at','12')->count();        

    $chart1 = \Chart::title([
    'text' => 'Daftar Transaksi Bulanan',
        ])
        ->chart([
            'type'     => 'line', // pie , columnt ect
            'renderTo' => 'chart1', // render the chart into your div with id
        ])
        ->subtitle([
            'text' => ' ',
        ])
        ->colors([
            '#0c2959'
        ])
        ->xaxis([
            'categories' => [
                'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember',
            ],
            'labels'     => [
                'rotation'  => 15,
                'align'     => 'top',
                'formatter' => 'startJs:function(){return this.value }:endJs', 
                // use 'startJs:yourjavasscripthere:endJs'
            ],
        ])
        ->yaxis([
            'text' => 'This Y Axis',
        ])
        ->legend([
            'layout'        => 'vertikal',
            'align'         => 'right',
            'verticalAlign' => 'middle',
        ])
        ->series(
            [
                [
                    'name'  => 'Jumlah Transaksi',
                    'data'  => [$data1, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10, $data11, $data12],
                    // 'color' => '#0c2959',
                ],
            ]
        )
        ->display();

    return view('dashboard_konven.dashboard',compact('populer','stok_habis','produk','pembeli','transaksi','hari_ini','chart1','saldo_hari_ini','gamapay_hari_ini','barang','pengeluaran_hari_ini','laba'));
  }
}
