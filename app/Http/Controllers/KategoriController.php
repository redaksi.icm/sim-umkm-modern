<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\BarangKonven;
use DB;

class KategoriController extends Controller
{
  public function __construct()
  {
      // $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Kategori::all()->sortBy('kategori');
        return view('kategori.list',compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      date_default_timezone_set('Asia/Jakarta');
      $this->validate(request(),
        [
          'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)|max:30',
          'deskripsi' => 'required|max:75',
        ],
        [
          'nama.required' => 'Nama kategori harus diisi!',
          'nama.regex' => 'Nama kategori tidak boleh berupa angka!',
          'nama.max' => 'Nama kategori terlalu panjang!',
          'deskripsi.required' => 'Deskripsi harus diisi!',
          'deskripsi.max' => 'Deskripsi terlalu panjang!',
        ]
      );
      Kategori::create([
            'kategori' => $request->nama,
            'deskripsi'=> $request->deskripsi,
      ]);
      
      return redirect('master/kategori')->with('success','Data berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data= Kategori::find($id);
      return view('kategori.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      date_default_timezone_set('Asia/Jakarta');
      $data= Kategori::where('id',$id)->first();
      $this->validate(request(),
      [
        'nama' => 'required|regex:(^\d*[a-zA-Z-][a-zA-Z\d\s-]*$)|max:30',
        'deskripsi' => 'required|max:75',
      ],
      [
        'nama.required' => 'Nama kategori harus diisi!',
        'nama.regex' => 'Nama kategori tidak boleh berupa angka!',
        'nama.max' => 'Nama kategori terlalu panjang!',
        'deskripsi.required' => 'Deskripsi harus diisi!',
        'deskripsi.max' => 'Deskripsi terlalu panjang!',
      ]);

      if ($data->kategori = $request->nama) {
        
      }

      $data= Kategori::find($id);
      $data->kategori=$request->nama;
      $data->deskripsi=$request->deskripsi;

      $data->save();

      return redirect('master/kategori')->with('success', 'Data berhasil diperbarui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $cek2 = DB::table('barang_konven')
              ->join('kategori', 'barang_konven.kategori_id', '=', 'kategori.id')
              ->where('kategori.id',$id)
              ->get();

      // remove id from KATEGORI
      if($cek2->isNotEmpty()){
            return redirect()->back()->with('error', 'Kategori yang dipilih masih memiliki data produk.');
      }else{

      $data = Kategori::find($id)->delete();

      return redirect('master/kategori')->with('success', 'Data kategori berhasil dihapus!');
      }
    }
}
