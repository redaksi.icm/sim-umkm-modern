<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\BarangExport;
use App\DetailTransaksi;
use App\StokBarang;
use Carbon\Carbon;
use App\Barang;
Use Alert;
use PDF;

class BarangReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $barang = Barang::all()->sortBy('nama');
      return view('barang-report.find',compact('barang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function laporan(Request $request)
    {
      if (Carbon::parse($request->from) > Carbon::parse($request->until)) {
        return redirect()->back()->with('error', 'Date not match!');
      }
      $this->validate(request(),
        [
          'barang' => 'required',
          'from' => 'required',
          'until' => 'required',
        ],
        [
          'barang.required' => 'Item can not be empty!',
          'from.required' => 'Date from can not be empty!',
          'until.required' => 'Date until can not be empty!',
        ]
      );
      $list = StokBarang::leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                              ->leftJoin('lokasi', 'barang_jual.lokasi_id', '=', 'lokasi.id')
                              ->leftJoin('barang', 'barang_jual.barang_id', '=', 'barang.id')
                              ->leftJoin('penjual', 'barang_jual.penjual_id', '=', 'penjual.id')
                              ->leftJoin('users', 'penjual.user_id', '=', 'users.id')
                              ->where('barang.id', $request->barang)
                              ->where('stok_barang.created_at', '>=',$request->from = Carbon::parse($request->from))
                              ->where('stok_barang.created_at', '<=',$request->until = Carbon::parse($request->until)->addDay(1))
                              ->select('stok_barang.id','users.name','stok_barang.created_at', 'stok_barang.harga', 'stok_barang.jumlah', 'lokasi.nama as lokasi')
                              ->get()
                              ->toArray();
      $solds = DetailTransaksi::leftJoin('stok_barang', 'detail_transaksi.stok_barang_id', '=', 'stok_barang.id')
                      ->leftJoin('transaksi', 'detail_transaksi.transaksi_id', '=', 'transaksi.id')
                      ->leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                      ->leftJoin('barang', 'barang_jual.barang_id', '=', 'barang.id')
                      ->leftJoin('penjual', 'barang_jual.penjual_id', '=', 'penjual.id')
                      ->groupBy('stok_barang.id', 'stok_barang.id', 'stok_barang.harga')
                      ->where('transaksi.status','=','success')
                      ->where('barang.id', $request->barang)
                      ->where('transaksi.updated_at', '>=', $request->from = Carbon::parse($request->from))
                      ->where('transaksi.updated_at', '<=', Carbon::parse($request->until))
                      ->selectRaw('stok_barang.id as stok_barang_id, sum(kuantitas)*stok_barang.harga as income, sum(kuantitas) as item')
                      ->get();
                      // dd($solds);
      $incomeTotal=0;
      $itemTotal=0;
      foreach ($solds as $key => $value) {
          $incomeTotal += $value['income'];
          $itemTotal += $value['item'];
      }

      // dd($solds);
      // dd($request);
      $barangAll = Barang::all()->sortBy('nama');
      $barang = Barang::where('id',$request->barang)->first();

      $collection = [];

      foreach ($list as $i => $stok) {
        $collection[$i] = [
          'id' => $stok['id'],
          'name' => $stok['name'],
          'harga' => $stok['harga'],
          'lokasi' => $stok['lokasi'],
          'jumlah' => $stok['jumlah'],
          'sold'  => 0,
          'remain' => $stok['jumlah'],
          'income' => 0,
          'created_at' => $stok['created_at']
          ];
        foreach ($solds as $j => $sold) {
          if ($sold->stok_barang_id == $stok['id']) {
            $collection[$i]['sold']  = $sold->item;
            $collection[$i]['remain']  = $stok['jumlah'] - $sold->item;
            $collection[$i]['income']  = $sold->item * $stok['harga'];
          }
        }
      }
      $requested = $request;
      // dd($request);
      return view('barang-report.laporan',compact('barang','list','collection', 'requested', 'barangAll', 'incomeTotal', 'itemTotal'));
      // dd($list);

    }

    public function downloadPDF(request $request){
      $list = StokBarang::leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                              ->leftJoin('lokasi', 'barang_jual.lokasi_id', '=', 'lokasi.id')
                              ->leftJoin('barang', 'barang_jual.barang_id', '=', 'barang.id')
                              ->leftJoin('penjual', 'barang_jual.penjual_id', '=', 'penjual.id')
                              ->leftJoin('users', 'penjual.user_id', '=', 'users.id')
                              ->where('barang.id', $request->barang)
                              ->where('stok_barang.created_at', '>=',$request->from = Carbon::parse($request->from))
                              ->where('stok_barang.created_at', '<=',$request->until = Carbon::parse($request->until))
                              ->select('stok_barang.id','users.name','stok_barang.created_at', 'stok_barang.harga', 'stok_barang.jumlah', 'lokasi.nama as lokasi')
                              ->get()
                              ->toArray();
      $solds = DetailTransaksi::leftJoin('stok_barang', 'detail_transaksi.stok_barang_id', '=', 'stok_barang.id')
                      ->leftJoin('transaksi', 'detail_transaksi.transaksi_id', '=', 'transaksi.id')
                      ->leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                      ->leftJoin('barang', 'barang_jual.barang_id', '=', 'barang.id')
                      ->leftJoin('penjual', 'barang_jual.penjual_id', '=', 'penjual.id')
                      ->groupBy('stok_barang.id', 'stok_barang.id', 'stok_barang.harga')
                      ->where('transaksi.status','=','success')
                      ->where('barang.id', $request->barang)
                      ->where('transaksi.updated_at', '>=', $request->from = Carbon::parse($request->from))
                      ->where('transaksi.updated_at', '<=', Carbon::parse($request->until))
                      ->selectRaw('stok_barang.id as stok_barang_id, sum(kuantitas)*stok_barang.harga as income, sum(kuantitas) as item')
                      ->get();
                      // dd($solds);
      $incomeTotal=0;
      $itemTotal=0;
      foreach ($solds as $key => $value) {
          $incomeTotal += $value['income'];
          $itemTotal += $value['item'];
      }
      // dd($solds);

      $barang = Barang::where('id',$request->barang)->first();

      $collection = [];

      foreach ($list as $i => $stok) {
        $collection[$i] = [
          'id' => $stok['id'],
          'name' => $stok['name'],
          'harga' => $stok['harga'],
          'lokasi' => $stok['lokasi'],
          'jumlah' => $stok['jumlah'],
          'sold'  => 0,
          'remain' => $stok['jumlah'],
          'income' => 0,
          'created_at' => $stok['created_at']
          //  = Carbon::parse($stok['created_at'])->format('l, j F Y'),
          ];
        foreach ($solds as $j => $sold) {
          if ($sold->stok_barang_id == $stok['id']) {
            $collection[$i]['sold']  = $sold->item;
            $collection[$i]['remain']  = $stok['jumlah'] - $sold->item;
            $collection[$i]['income']  = $sold->item * $stok['harga'];
          }
        }
      }
      // dd($request);
      // dd($collection);
      // return view('barang-report.pdf',compact('barang','list','collection', 'request', 'incomeTotal', 'itemTotal'));
      $pdf = PDF::loadView('barang-report.pdf', compact('barang','list','collection', 'request', 'incomeTotal', 'itemTotal'));
      $request->until = Carbon::parse($request->until)->addDay(-1)->format('dmY');
      $request->from = Carbon::parse($request->from)->format('dmY');
      return $pdf->download('report-'.$barang->nama.'-'.$request->from.'-'.$request->until.'.pdf');

    }

    public function downloadExcel(Request $request)
    {
      // dd($request);
      $list = StokBarang::leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                              ->leftJoin('lokasi', 'barang_jual.lokasi_id', '=', 'lokasi.id')
                              ->leftJoin('barang', 'barang_jual.barang_id', '=', 'barang.id')
                              ->leftJoin('penjual', 'barang_jual.penjual_id', '=', 'penjual.id')
                              ->leftJoin('users', 'penjual.user_id', '=', 'users.id')
                              ->where('barang.id', $request->barang)
                              ->where('stok_barang.created_at', '>=',$request->from = Carbon::parse($request->from))
                              ->where('stok_barang.created_at', '<=',$request->until = Carbon::parse($request->until))
                              ->select('stok_barang.id','users.name','stok_barang.created_at', 'stok_barang.harga', 'stok_barang.jumlah', 'lokasi.nama as lokasi')
                              ->get()
                              ->toArray();

      $solds = DetailTransaksi::leftJoin('stok_barang', 'detail_transaksi.stok_barang_id', '=', 'stok_barang.id')
                      ->leftJoin('transaksi', 'detail_transaksi.transaksi_id', '=', 'transaksi.id')
                      ->leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                      ->leftJoin('barang', 'barang_jual.barang_id', '=', 'barang.id')
                      ->leftJoin('penjual', 'barang_jual.penjual_id', '=', 'penjual.id')
                      ->groupBy('stok_barang.id', 'stok_barang.id', 'stok_barang.harga')
                      ->where('transaksi.status','=','success')
                      ->where('barang.id', $request->barang)
                      ->where('transaksi.updated_at', '>=', $request->from = Carbon::parse($request->from))
                      ->where('transaksi.updated_at', '<=', Carbon::parse($request->until))
                      ->selectRaw('stok_barang.id as stok_barang_id, sum(kuantitas)*stok_barang.harga as income, sum(kuantitas) as item')
                      ->get();
      // dd($solds);

      $barang = Barang::where('id',$request->barang)->first();

      $collection = [];

      foreach ($list as $i => $stok) {
        $collection[$i] = [
          'created_at' => $stok['created_at'],
          'lokasi' => $stok['lokasi'],
          'name' => $stok['name'],
          'harga' => $stok['harga'],
          'jumlah' => $stok['jumlah'],
          // 'id' => $stok['id'],
          'sold'  => 0,
          'remain' => $stok['jumlah'],
          'income' => 0,
          //  = Carbon::parse($stok['created_at'])->format('l, j F Y'),
          ];
        foreach ($solds as $j => $sold) {
          if ($sold->stok_barang_id == $stok['id']) {
            $collection[$i]['sold']  = $sold->item;
            $collection[$i]['remain']  = $stok['jumlah'] - $sold->item;
            $collection[$i]['income']  = $sold->item * $stok['harga'];
          }
        }
      }
      // dd($collection, $list);
      $request->until = Carbon::parse($request->until)->addDay(-1)->format('dmY');
      $request->from = Carbon::parse($request->from)->format('dmY');
      $exporter = app()->makeWith(BarangExport::class, compact('collection'));
      return $exporter->download('report-'.$barang->nama.'-'.$request->from.'-'.$request->until.'.xlsx');
    }

}
