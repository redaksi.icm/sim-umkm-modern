<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Alert;
Use App\TransaksiKonven;
Use App\DetailTransaksiKonven;
use Carbon\Carbon;
use App\User;
use App\Pembeli;
use App\Keranjang;
use App\StokBarang;
use Auth;
use DB;
use Excel;

class TransaksiController extends Controller
{
  public function __construct()
  {
      // $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function find(Request $request)
    {
      if (Carbon::parse($request->from) > Carbon::parse($request->until)) {
        return redirect()->back()->with('error', 'Tanggal tidak cocok!');
      }
      $this->validate(request(),
        [
          'from' => 'required',
          'until' => 'required',
        ],
        [
          'from.required' => 'Tanggal tidak boleh kosong!',
          'until.required' => 'Tanggal tidak boleh kosong!',
        ]
      );
      $data = DB::table('transaksi_konven')
                ->join('jenis_pembayaran', 'transaksi_konven.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                ->join('pembeli', 'transaksi_konven.pembeli_id', '=', 'pembeli.id')
                ->join('penjual_konven', 'transaksi_konven.penjual_konven_id', '=', 'penjual_konven.id')
                ->select('transaksi_konven.*','jenis_pembayaran.jenis_pembayaran','pembeli.nama','penjual_konven.nama_toko')
                ->where('transaksi_konven.updated_at', '>=', $request->from = Carbon::parse($request->from))
      ->where('transaksi_konven.updated_at', '<=', $request->until = Carbon::parse($request->until)->addDay(1))
      ->get();
      // dd($data);

      return view('transaksi_admin.list', compact('data','request'));
    }

    public function index(Request $request)
    {
      return view('transaksi_admin.find', compact('data','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
      $data = DB::table('detail_transaksi_konven')
                ->join('barang_konven', 'detail_transaksi_konven.barang_konven_id', '=', 'barang_konven.id')
                ->select('detail_transaksi_konven.*','barang_konven.nama')
                ->where('transaksi_konven_id', $id)
                ->get();
      $transaksi = DB::table('transaksi_konven')
                ->where('id', $id)
                ->pluck('id')->first();
      // dd($data);
      return view('transaksi_admin.detail', compact('data','transaksi'));
    }

    //print nota
    public function getPdf($id)
    {
        date_default_timezone_set('Asia/Jakarta');       

        $value = TransaksiKonven::with('pembeli','penjual_konven')->find($id);
        $data = DetailTransaksiKonven::with('transaksiKonven','barang_konven')->where('transaksi_konven_id',$id)->get()->toArray();

        return view('kasir_utama.nota',compact('value','data'));
    }

    public function exportExcel(Request $request)
    {
        $path1 = $request->get('tgl_awal');
        $path = $request->get('tgl_akhir');

        if(!empty($path1) && !empty($path)){

            $user = Auth::user()->penjual_konven()->pluck('id')->first();

            $data = DB::table('transaksi_konven')
                ->join('jenis_pembayaran', 'transaksi_konven.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                ->join('pembeli', 'transaksi_konven.pembeli_id', '=', 'pembeli.id')
                ->join('penjual_konven', 'transaksi_konven.penjual_konven_id', '=', 'penjual_konven.id')
                ->select('transaksi_konven.id','penjual_konven.nama_toko','pembeli.nama','jenis_pembayaran.jenis_pembayaran','transaksi_konven.status','transaksi_konven.diskon','transaksi_konven.total','transaksi_konven.created_at')
                ->whereBetween('transaksi_konven.updated_at',[$path1,$path])
                ->get();

            $data= json_decode( json_encode($data), true);
            Excel::create('Data Transaksi', function($excel) use($data){
            $excel->sheet('Data Transaksi', function ($sheet) use ($data) {
                $sheet->fromArray($data);
                });
            })->download("xlsx");
        }

        else{

            $data = DB::table('transaksi_konven')
                ->join('jenis_pembayaran', 'transaksi_konven.jenis_pembayaran_id', '=', 'jenis_pembayaran.id')
                ->join('pembeli', 'transaksi_konven.pembeli_id', '=', 'pembeli.id')
                ->join('penjual_konven', 'transaksi_konven.penjual_konven_id', '=', 'penjual_konven.id')
                ->select('transaksi_konven.id','penjual_konven.nama_toko','pembeli.nama','jenis_pembayaran.jenis_pembayaran','transaksi_konven.status','transaksi_konven.diskon','transaksi_konven.total','transaksi_konven.created_at')
                ->get();

            $data= json_decode( json_encode($data), true);
            Excel::create('Data Transaksi', function($excel) use($data){
            $excel->sheet('Data Transaksi', function ($sheet) use ($data) {
                $sheet->fromArray($data);
                });
            })->download("xlsx");

        }
    }

// START OF API Controller
public function api_index()
    {
      $transaksi = Transaksi::with('pembeli','jenisPembayaran','detailTransaksi')->get();
      return response()->json([
        'status'=>'success',
        'result'=> $transaksi,
      ]);
    }

     public function api_store(Request $request)
     {
       DB::beginTransaction();
       if (Keranjang::where('pembeli_id', User::find(Auth::user()->id)->pembeli()->first()->id)->count() > 0) {
         $pembeli = User::find(Auth::user()->id)->pembeli()->first()->id;
         $keranjang = Keranjang::leftJoin('stok_barang', 'keranjang.stok_barang_id', '=', 'stok_barang.id')
                            ->leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                            ->where('pembeli_id', $pembeli)
                            ->select('keranjang.pembeli_id','stok_barang.stok','stok_barang.id')
                            ->get()->toArray();
                            // dd($keranjang);
         $detail_transaksi = Keranjang::leftJoin('stok_barang', 'keranjang.stok_barang_id', '=', 'stok_barang.id')
                            ->leftJoin('barang_jual', 'stok_barang.barang_jual_id', '=', 'barang_jual.id')
                            ->where('pembeli_id', $pembeli)
                            ->select('stok_barang_id','kuantitas','stok_barang.harga as total_bayar')
                            ->get()->toArray();
         foreach ($detail_transaksi as $key => $value) {
           // foreach ($keranjang as $key1 => $value1) {
           //   $keranjang[$key1]['stok'] = $keranjang[$key1]['stok'] - $detail_transaksi[$key1]['kuantitas'];
           // }
           // if($keranjang[$key1]['stok'] < 0){
           //   return response()->json([
           //     'status'=>'failed',
           //     'error'=>'Something wrong!',
           //     'message'=>'insufficient stock!',
           //   ]);
           $detail_transaksi[$key]['total_bayar'] = $detail_transaksi[$key]['total_bayar'] * $detail_transaksi[$key]['kuantitas'];
         }

         $detTrans = Transaksi::create([
           'id' => rand(),
           'pembeli_id'=> $pembeli,
           'jumlah_bayar'=> $request->jumlah_bayar,
           'status'=> 'Waiting'
           ])->detailTransaksi()->createMany($detail_transaksi);
           if (!$detTrans) {
             DB::rollback();
             return response()->json([
               'status'=>'failed',
               'error'=>'Something wrong!',
               'message'=>'Something wrong!',
             ]);
           }
           // dd($detTrans);
           foreach ($detTrans as $key => $value) {
             $value->logStok()->create([
               'stok_barang_id'=>$value->stok_barang_id,
               'stok_awal'=>$value->stokBarang->stok,
               'type'=>'transaction',
               'total'=>$value->kuantitas,
               'stok_update'=>$value->stokBarang->stok - $value->kuantitas
             ]);
             if (!$value) {
               DB::rollback();
               return response()->json([
                 'status'=>'failed',
                 'error'=>'Something wrong!',
                 'message'=>'Something wrong!',
               ]);
             }
             $stokBarang = StokBarang::where('id', $value->stokBarang->id)->first();
             $stokBarang->stok = $stokBarang->stok - $value->kuantitas;
             $stokBarang->save();
             if (!$stokBarang) {
               DB::rollback();
               return response()->json([
                 'status'=>'failed',
                 'error'=>'Something wrong!',
                 'message'=>'Something wrong!',
               ]);
             }
           }
           $trans = Transaksi::find($detTrans[0]->transaksi_id);

           $delKeranjang = Keranjang::where('pembeli_id', $pembeli)->delete();
           DB::commit();
           return response()->json([
             'status'=>'success',
             'result'=>$trans
           ]);
       }
       else
       {
         return response()->json([
           'status'=>'success',
           'result'=>'chart is empty'
         ]);
       }
     }

     public function api_pay(Request $request, $id)
     {
       DB::beginTransaction();

       $trans= Transaksi::find($id);
       // dd($trans);
       $detailTrans = DetailTransaksi::where('transaksi_id', $id)->get();
       // return response()->json([
       //   'status'=>$detailTrans
       // ]);
       // dd($trans->status);
       $pembeli= User::where('id', Auth::user()->id)->first();
      if($trans->status == 'Success') {
        return response()->json([
          'status'=>'failed',
          'error'=>'Your transaction has been paid',
          'message'=>'Your transaction has been paid',
        ]);
      }
      elseif($trans->status == 'Failed') {
        return response()->json([
          'status'=>'failed',
          'error'=>'Your transaction has been failed',
          'message'=>'Your transaction has been failed',
        ]);
      }
      elseif ($trans->status == 'Waiting') {
        // PAYMENT MENGGUNAKAN PAY-JUR
        if ($request->jenis_pembayaran_id == 1) {
          if($pembeli->saldo < $trans->jumlah_bayar) {
            return response()->json([
              'status'=>'failed',
              'error'=>'Saldo not enough',
              'message'=>'Saldo not enough',
            ]);
          }
          $trans->jenis_pembayaran_id= 1;
          $trans->status='Success';
          $trans->save();
          if (!$trans) {
            DB::rollback();
            return response()->json([
              'status'=>'failed',
              'error'=>'Something wrong!',
              'message'=>'Something wrong!',
            ]);
          }
          $trans->logSaldo()->create([
            'user_id'=>$pembeli->id,
            'saldo_awal'=>$pembeli->saldo,
            'type'=>'purchase',
            'total'=>$trans->jumlah_bayar,
            'grand_total'=>$pembeli->saldo - $trans->jumlah_bayar
          ]);
          if (!$trans) {
            DB::rollback();
            return response()->json([
              'status'=>'failed',
              'error'=>'Something wrong!',
              'message'=>'Something wrong!',
            ]);
          }
          $pembeli->saldo = $trans->logSaldo->grand_total;
          $pembeli->save();
          if (!$pembeli) {
            DB::rollback();
            return response()->json([
              'status'=>'failed',
              'error'=>'Something wrong!',
              'message'=>'Something wrong!',
            ]);
          }
          foreach ($detailTrans as $key => $value) {
            $value->logSaldo()->create([
              'user_id'=>$value->stokBarang->barangJual->penjual->user->id,
              'saldo_awal'=>$value->stokBarang->barangJual->penjual->user->saldo,
              'type'=>'sales',
              'total'=>$value->total_bayar,
              'grand_total'=>$value->stokBarang->barangJual->penjual->user->saldo + $value->total_bayar
            ]);
            if (!$value) {
              DB::rollback();
              return response()->json([
                'status'=>'failed',
                'error'=>'Something wrong!',
                'message'=>'Something wrong!',
              ]);
            }
            $penjual = User::where('id', $value->stokBarang->barangJual->penjual->user->id)->first();
            // dd($penjual);
            $penjual->saldo += $value->total_bayar;
            $penjual->save();
            if (!$penjual) {
              DB::rollback();
              return response()->json([
                'status'=>'failed',
                'error'=>'Something wrong!',
                'message'=>'Something wrong!',
              ]);
            }
          }
          DB::commit();
          return response()->json([
            'status'=>'success',
            'pembeli'=>$pembeli,
            'result'=>$trans,
          ]);
        }
        // PAYMENT MENGGUNAKAN SELAIN PAY-JUR
        else {
          $trans->jenis_pembayaran_id = $request->jenis_pembayaran_id;
          $trans->status='Waiting';
          $trans->save();
          DB::commit();
          return response()->json([
            'status'=>'success',
            'transaksi'=>$trans,
            'pembeli'=>$pembeli
          ]);
        }
      }
    }

// Panggil fungsi ini ketika pembayar pakai tap, dan sudah 10 menit
   public function api_transactionFailed($id){
     $trans= Transaksi::find($id);
     $trans->status='Failed';
     // dd($trans->detailTransaksi);
     foreach ($trans->detailTransaksi as $key => $value) {
       $value->logStok()->create([
         'stok_barang_id'=>$value->stok_barang_id,
         'stok_awal'=>$value->stokBarang->stok,
         'type'=>'failed',
         'total'=>$value->kuantitas,
         'stok_update'=>$value->stokBarang->stok + $value->kuantitas
       ]);
       $stokBarang = StokBarang::where('id', $value->stokBarang->id)->first();
       $stokBarang->stok = $stokBarang->stok + $value->kuantitas;
       $stokBarang->save();
     }
     $trans->save();
     return response()->json([
       'status'=>'success',
       'result'=>$trans,
     ]);
   }

// Panggil fungsi ini ketika pembayar pakai cash, dan konfirmasi jika sudah membayar
   public function api_transactionSuccess($id){
     DB::beginTransaction();
     $detailTrans = DetailTransaksi::where('transaksi_id', $id)->get();
     $trans= Transaksi::find($id);
     $trans->status='Success';
     $trans->save();
     if (!$trans) {
       DB::rollback();
       return response()->json([
         'status'=>'failed',
         'error'=>'Something wrong!',
         'message'=>'Something wrong!',
       ]);
     }
     foreach ($detailTrans as $key => $value) {
       $value->logSaldo()->create([
         'user_id'=>$value->stokBarang->barangJual->penjual->user->id,
         'saldo_awal'=>$value->stokBarang->barangJual->penjual->user->saldo,
         'type'=>'sales',
         'total'=>$value->total_bayar,
         'grand_total'=>$value->stokBarang->barangJual->penjual->user->saldo + $value->total_bayar
       ]);
       $penjual = User::where('id', $value->stokBarang->barangJual->penjual->user->id)->first();
       $penjual->saldo += $value->total_bayar;
       $penjual->save();
       if (!$penjual) {
         DB::rollback();
         return response()->json([
           'status'=>'failed',
           'error'=>'Something wrong!',
           'message'=>'Something wrong!',
         ]);
       }
     }
     DB::commit();
     return response()->json([
       'status'=>'success',
       'result'=>$trans,
     ]);
   }

   public function api_detail($id)
   {
         $trans= Transaksi::with('pembeli','jenisPembayaran','detailTransaksi.stokBarang.barangJual.barang')->find($id);
         return response()->json([
           'status'=>'success',
           'result'=>$trans
         ]);
   }

   public function api_riwayat()
   {
         $riwayat = Transaksi::leftJoin('pembeli', 'transaksi.pembeli_id', '=', 'pembeli.id')
                              ->leftJoin('users', 'pembeli.user_id', '=', 'users.id')
                              ->where('users.id', Auth::user()->id)
                              // ->where('transaksi.status', 'Success')
                              // ->orWhere('transaksi.status', 'Failed')
                              ->select('transaksi.*')
                              ->get();
         return response()->json([
           'status'=>'success',
           'result'=>$riwayat
         ]);
   }

   public function api_pilihMetode()
   {
     $data = Transaksi::leftJoin('pembeli', 'transaksi.pembeli_id', '=', 'pembeli.id')
                         ->leftJoin('users', 'pembeli.user_id', '=', 'users.id')
                         ->where('users.id', Auth::user()->id)
                         ->whereNull('transaksi.jenis_pembayaran_id')
                         ->select('transaksi.*')
                         ->get();
     if (empty($data)) {
       return response()->json([
         'status'=>'success',
         'result'=>'empty'
       ]);
     }
     return response()->json([
       'status'=>'success',
       'result'=>$data
     ]);
   }

   public function api_waiting()
   {
     $data = Transaksi::leftJoin('pembeli', 'transaksi.pembeli_id', '=', 'pembeli.id')
                          ->leftJoin('users', 'pembeli.user_id', '=', 'users.id')
                          ->where('users.id', Auth::user()->id)
                          ->where('transaksi.status', 'Waiting')
                          ->whereNotNull('transaksi.jenis_pembayaran_id')
                          ->select('transaksi.*')
                          ->get();
    // dd($data);
    if (empty($data)) {
      return response()->json([
        'status'=>'success',
        'result'=>'empty'
      ]);
    }
     return response()->json([
       'status'=>'success',
       'result'=>$data
     ]);
   }

}
