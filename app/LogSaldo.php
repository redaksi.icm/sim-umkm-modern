<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogSaldo extends Model
{
  protected $table = 'log_saldo';
  protected $guarded = ['created_at', 'updated_at'];
  public $timestamps = true;

  public function user()
  {
    return $this->belongsTo('App\User', 'user_id', 'id');
  }
  public function topup()
  {
    return $this->morphTo();
  }
  public function pencairan()
  {
    return $this->morphTo();
  }
  public function detailTransaksi()
  {
    return $this->morphTo();
  }
  public function transaksi()
  {
    return $this->morphTo();
  }
}
