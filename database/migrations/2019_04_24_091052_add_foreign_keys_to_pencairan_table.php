<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPencairanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pencairan', function(Blueprint $table)
		{
			$table->foreign('bank_id', 'pencairan_ibfk_1')->references('id')->on('bank')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pencairan', function(Blueprint $table)
		{
			$table->dropForeign('pencairan_ibfk_1');
			$table->dropForeign('pencairan_user_id_foreign');
		});
	}

}
