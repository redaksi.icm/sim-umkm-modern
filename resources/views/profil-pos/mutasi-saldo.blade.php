@section('js')

<script type="text/javascript">$('#app').fadeTo(300,1).fadeOut(1000);</script>

@stop
@extends('layout_pos')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Mutasi Saldo
        <small>Gamapay</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-pencil"></i> Profil</a></li>
        <li class="active">Mutasi Saldo</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div><br>
          
          @if(Session::has('success'))
            <div id="app" class="alert alert-success">
              {{ Session::get('success') }}
            </div>
          @elseif(Session::has('error'))
            <div class="alert alert-error">
              {{ Session::get('error') }}
            </div>
          @endif
        </div>      

    <div class="exportexcel">
        <a href="#" class="btn btn-primary"><i class="fa fa-plus mg-r-10"></i> Tambah Barang</a>
        <a class="btn btn-success" href="#"><i class="fa fa-file-excel-o"></i>Export Excel</a>
        
    </div><br>
    
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Produk</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               
             <table id="example2" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Kategori</th>
                    <th>Deskripsi</th>
                    <th>Harga</th>
                    <th>Stok</th>
                    <th>Diskon</th>
                    <th width="10%">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($list as $value => $brg)
                  <tr>
                    <td align="center">{{$value +1}}</td>
                    <td>{{$brg->nama}}</td>
                    <td>{{$brg->kategori}}</td>
                    <td>{{$brg->deskripsi}}</td>
                    <td>{{$brg->harga}}</td>
                    <td>{{$brg->stok}}</td>
                    <td>{{$brg->diskon}}</td>
                    <td>
                      <a href="{{url('barang_konven/edit_pos/'.$brg->id.'')}}" class="btn btn-warning btn-icon rounded-circle mg-r-5 mg-b-10" title="Edit">
                        <div><i class="fa fa-pencil"></i></div>
                      </a>
                      <a href="javascript:if(confirm('Yakin ingin hapus data?')){window.location.href='{{url('barang_konven/delete/'.$brg->id.'')}}'};"
                      class="btn btn-danger btn-icon rounded-circle mg-r-5 mg-b-10" title="Delete">
                        <div><i class="fa fa-trash"></i></div>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
            </table>
            </div>
            <!-- ./box-body -->
            <div class="box-footer" align="center">
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    
    <br><br><br> 

    </section>    
    
@endsection
