@extends('master')

@section('css')
<!-- Datatables -->
    <link href="{{asset('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Home</a> /
  <a href="{{url('#')}}">Gamapay</a> /
  <a href="{{url('/topup')}}">Topup</a> /
  <a href="{{url('#')}}">Pengguna</a>

        <div><br>
          @if(Session::has('success'))
            <div id="app" class="alert alert-success">
              {{ Session::get('success') }}
            </div>
          @elseif(Session::has('error'))
            <div class="alert alert-error">
              {{ Session::get('error') }}
            </div>
          @endif
        </div> 
@stop   

@section('title')
  <h3>Gamapay</h3>
@stop

@section('content')
  <div class="x_panel">
    <div class="x_title">
      <h2>Topup</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <div class="col-md-2 col-sm-2 col-xs-12 profile_left" style="margin-bottom:10px">
        <div class="profile_img">
          <div id="crop-avatar">
            <img class="img-responsive img-circle avatar-view" src="{{asset('images/'.$data->foto.'')}}" alt="Avatar" title="Change the avatar">
          </div>
        </div>
      </div>
      <div class="col-md-5 col-sm-5 col-xs-12 profile_left">
        <label>GENERAL</label>
        <table class="table display responsive nowrap">
          <tbody>
            <tr>
              <td class="col-md-4" style="text-align:right"> Email:</td>
              <td>{{$data->email}}</td>
            </tr>
            <tr>
              <td style="text-align:right"> Nama:</td>
              <td>{{$data->name}}</td>
            </tr>
            <tr>
              <td style="text-align:right"> Nomor Telepon:</td>
              <td>{{$data->no_telepon}}</td>
            </tr>
            <tr>
              <td style="text-align:right"> Tanggal Lahir:</td>
              <td>{{\Carbon\Carbon::parse($data->tanggal_lahir)->format('j F Y')}}</td>
            </tr>
          </tbody>
        </table>
      </div>
      {!! Form::open(['url'=>'topup/'.$data->id.'/tambah-saldo', 'role'=>'form', 'files'=>true, 'class'=>'form-horizontal form-label-left', 'novalidate'])!!}
      <div class="col-md-5 col-sm-5 col-xs-12">
        <div class="item form-group">
          <label class="control-label col-md-4 col-sm-4 col-xs-12" for="name">Jumlah Saldo</label>
          <div class="col-md-8 col-sm-8 col-xs-12">
            <input readonly type="text" placeholder="" name="saldo" value="Rp {{number_format($data->saldo,0,".",".")}},-"  class="form-control col-md-7 col-xs-12">
            <input type="hidden" placeholder="" name="user_id" value="{{$data->id}}"  class="form-control col-md-7 col-xs-12">
          </div>
        </div>
        <div class="item form-group">
          <label style="margin-top:10px" class="control-label col-md-4 col-sm-4 col-xs-12" for="name">Top Up <span class="required">*</span>
          </label>
          <div style="margin-top:10px" class="col-md-8 col-sm-8 col-xs-12">
            <input type="number" class="form-control" placeholder="topup" name="total" value="{{old('total')}}">
            @if ($errors->has('total'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('total') }}</strong>
              </span>
            @endif
          </div>
        </div>
        <div class="form-group">
          <div style="margin-top:10px" class="col-md-8 col-md-offset-3 pull-right">
            <a class="btn btn-primary" onclick="location.href='{{url('topup')}}'">Batal</a>
            <button id="send" type="submit" class="btn btn-success">Simpan</button>
          </div>
        </div>
      </div>
      {!!Form::close()!!}
      <div class="clearfix"></div>
    </div>
  </div>

  {{-- <div class="x_panel">
    <div class="x_title">
      <h2>History of Pay-Jur</h2>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <div class="col-md-12 col-sm-12 col-xs-12 profile_left">
        <table id="datatable1" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="col-md-1">No</th>
              <th>Date</th>
              <th>Time</th>
              <th>Total</th>
              <th>Type</th>
            </tr>
          </thead>
          <tbody>
            @foreach($history as $value => $hist)
            <tr>
              <td class="col-md-1" align="center">{{$value +1}}</td>
              <td>{{\Carbon\Carbon::parse($hist->created_at)->format('l, j F Y')}}</td>
              <td>{{\Carbon\Carbon::parse($hist->created_at)->format('H:i:s')}}</td>
              <td>{{$hist->total}}</td>
              <td>{{$hist->type}}</td>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      <div class="clearfix"></div>
    </div>
  </div> --}}
@endsection

@section('javascript')
      <script src="{{asset('assets/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
      <script src="{{asset('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
      <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      <script src="{{asset('assets/vendors/select2/dist/js/select2.min.js')}}"></script>
      <script src="{{asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
      <script type="text/javascript">$('#app').fadeTo(300,1).fadeOut(3000);</script>
@endsection
