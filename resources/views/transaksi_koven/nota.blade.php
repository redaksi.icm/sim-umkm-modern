@section('js')
<script type="text/javascript">$('#app').fadeTo(300,1).fadeOut(1000);</script>
@stop

<div><br>          
          @if(Session::has('success'))
            <div id="app" class="alert alert-success">
              {{ Session::get('success') }}
            </div>
          @elseif(Session::has('error'))
            <div class="alert alert-error">
              {{ Session::get('error') }}
            </div>
          @endif
</div>

<link rel="shortcut icon" href="{{ asset('image/logo-ugm.png')}}">
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}" type="text/css" />


<div class="container">
<div class="row-fluid">
<div class="span12">
  <table width="100%" border="0" class="">
    <tr>
      <td width="11%" height="162" align="center"></td>
      <td width="77%" align="center"><h2>NOTA BELANJA</h2>
        <h3 align="center"><strong>{{$value->penjual_konven->nama_toko}}</strong></h3>
        <p align="center">{{ date('d-m-Y H:i:s') }}</p>
        <p align="center">Pembeli : {{$value->pembeli->nama}}</p>
        <p align="center"><strong>===========================================================================</strong></p></td>
	     
      <td width="12%" align="center"></td>
	
    </tr>
	
  </table>
  
</div>
</div>

<center><h3>Daftar Belanja</h3></center>
<p>&nbsp;</p>
<a class="btn btn-medium btn-primary" href="#" onClick="window.print();"><i class="icon-print"></i> Cetak</a>
<a class="btn btn-medium btn-success" href="{{ URL::previous() }}"><i class="icon-print"></i> Selesai</a>
<p>&nbsp;</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
<thead>
<th>Barang</th>
<th>Jmlh</th>
<th>Harga</th>
<th>Diskon</th>
<th>Sub Total</th>
</thead>
<tbody>
        @foreach($data as $item)
        <tr>
            <td>{{$item['barang_konven']['nama']}}</td>
            <td>{{$item['jumlah']}}</td>
            <td>{{$item['harga']}}</td>
            <td>{{$item['diskon']}}</td>
            <td align="left">{{$item['total']}}</td>
        </tr>
        @endforeach
</tbody>
<tfoot>

        <tr>
            <td colspan="1"></td>
            <td colspan="1"></td>
            <td colspan="1"></td>
            <td align="left"><b>Total</b></td>
            <td align="left" class="gray"><b><i>{{$item['transaksi_konven']['total']}}</i></b></td>
        </tr>
            
</tfoot>
</table>