@section('js')

<!-- untuk ajax yang ambil data -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<!-- untuk auto complete -->
<link href="http://demo.expertphp.in/css/jquery.ui.autocomplete.css" rel="stylesheet">
<script src="http://demo.expertphp.in/js/jquery.js"></script>
<script src="http://demo.expertphp.in/js/jquery-ui.min.js"></script>

@stop
@extends('layout_pos')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transaksi Baru
        <small>Gamapay</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-pencil"></i> Transaksi</a></li>
        <li class="active">Transaksi Baru</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div><br>
          
          @if(Session::has('success'))
            <div id="app" class="alert alert-success">
              {{ Session::get('success') }}
            </div>
          @elseif(Session::has('error'))
            <div class="alert alert-error">
              {{ Session::get('error') }}
            </div>
          @endif
        </div>

      <div class="row"> 

        <div class="col-md-3">
          
        </div>
        <!-- /.col -->

        <div class="col-md-3">

          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Cara pembayaran Gamapay</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                  <div class="col col-md-12">

                        1. Buka aplikasi Gamapay<br>
                        2. Buka bagian Scan QR-Code<br>
                        3. Scan QR-Code yang tertera di layar kasir toki<br>
                        4. Masukan pin untuk melakukan pembayaran<br>
                        5. Selesai<br>
                  </div>                                   
                </div>
            </div>
            <!-- ./box-body -->
            <div class="box-footer" align="center">
              Gamapay UGM
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
          
        </div>      

        <div class="col-md-3">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Pembayaran Gamapay</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body" align="center">

                <div class="row">
                      
                        {!! QrCode::size(200)->generate( $getId ); !!}
                        <p>Scan untuk melakukan pembayaran.</p>
                                   
                </div>
            </div>
            <!-- ./box-body -->
            <div class="box-footer" align="center">
              <a href="{{ URL::previous() }}" type="button" name="kembali" id="kembali" href="#" class="btn btn-primary">Selesai</a>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

        <div class="col-md-3">
          
        </div>
        <!-- /.col -->

        
        
      </div>
      <!-- /.row -->      

    </section>    
    
@endsection
