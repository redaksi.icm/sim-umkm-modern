@extends('layout_pos')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daftar Detail Transaksi
        <small>{{Auth::user()->name}}</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-pencil"></i> Dartar Detail Transaksi</a></li>
        <li class="active">{{Auth::user()->name}}</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div><br>
          
          @if(Session::has('success'))
            <div id="app" class="alert alert-success">
              {{ Session::get('success') }}
            </div>
          @elseif(Session::has('error'))
            <div class="alert alert-error">
              {{ Session::get('error') }}
            </div>
          @endif
        </div>

        <a class="btn btn-medium btn-primary" href="{{url('invoice/'.$transaksi.'')}}"><i class="icon-print"></i> Print Nota</a><br><br>
        <!-- <a class="btn btn-medium btn-primary" href="{{url('transaksi_konven/backToist')}}"><i class="icon-print"></i> Kembali</a><br><br> -->

        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Daftar Detail Transaksi</h3>

              </div>
              <!-- /.box-header -->
              <div class="box-body">                  
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Barang</th>
                  <th>Id Transaksi</th>
                  <th>Harga</th>
                  <th>Diskon</th>
                  <th>jumlah</th>
                  <th>Total</th>
                  <th>THB</th>
                  <th>Tanggal</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @foreach($list as $value => $transaksi)
                <tr>
                  <td align="center">{{$value+1}}</td>
                  <td>{{$transaksi->nama}}</td>
                  <td>{{$transaksi->transaksi_konven_id}}</td>
                  <td>{{$transaksi->harga}}</td>
                  <td>{{$transaksi->diskon}}</td>
                  <td>{{$transaksi->jumlah}}</td>
                  <td>{{$transaksi->total}}</td>
                  <td>{{$transaksi->total_beli}}</td>
                  <td>{{\Carbon\Carbon::parse($transaksi->updated_at)->formatLocalized('%A, %d %B %Y')}}</td>
                  <td>
                    <a data-toggle="modal" data-target="#myModal{{ $transaksi->id }}" data-id="{{ $transaksi->id }}" class="btn btn-warning">Edit</a>
                    <a href="javascript:if(confirm('Yakin ingin hapus data?')){window.location.href='{{url('transaksi_konven/detail/delete/'.$transaksi->id.'')}}'};"
                    class="btn btn-danger btn-icon rounded-circle mg-r-5 mg-b-10" title="Delete">
                      <div><i class="fa fa-trash"></i></div>
                    </a>
                  </td>
                </tr>
                <!-- Modal -->
                <div class="modal fade" id="myModal{{ $transaksi->id }}" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                    <form action="{{url('transaksi_konven/update_detail_pos/'.$transaksi->id.'')}}" method="post" enctype="multipart/form-data">
                      {{csrf_field()}}
                      <div class="modal-header">
                        <a class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></a>
                        <center><h2 class="modal-title" id="myModalLabel"><i class="fa fa-exclamation-circle"></i> Edit Data</h2></center>
                      </div>                  
                      <div class="modal-body">        
                        <div class="form-group">
                          <label for="jumlah">Jumlah Barang<span class="required" style="color:red">*</span></label>
                          <input class="form-control" placeholder="Jumlah Barang" name="jumlah" id="jumlah{{ $transaksi->id }}" value="{{(isset($transaksi->jumlah))?$transaksi->jumlah:''}} " type="text">
                          @if ($errors->has('jumlah'))
                          <span class="invalid-feedback" role="alert" style="color: red">
                            <strong>{{ $errors->first('jumlah') }}</strong>
                          </span>
                          @endif
                        </div>
                        <div class="form-group">
                          <label for="harga">Harga<span class="required" style="color:red">*</span></label>
                          <input class="form-control" placeholder="Harga" name="harga" id="harga{{ $transaksi->id }}" value="{{(isset($transaksi->harga))?$transaksi->harga:''}} " type="text">
                        </div>
                        <div class="form-group">
                          <label for="harga_beli">Harga Beli<span class="required" style="color:red">*</span></label>
                          <input class="form-control" placeholder="Harga Beli" name="harga_beli" id="harga_beli{{ $transaksi->id }}" value="{{(isset($transaksi->harga_beli))?$transaksi->harga_beli:''}} " type="text">
                        </div>
                        <div class="form-group">
                          <label for="total">Total<span class="required" style="color:red">*</span></label>
                          <input class="form-control" placeholder="Total" name="total" id="total{{ $transaksi->id }}" value="{{(isset($transaksi->total))?$transaksi->total:''}} " type="text">
                        </div>
                        <div class="form-group">
                          <label for="thb">THB<span class="required" style="color:red">*</span></label>
                          <input class="form-control" placeholder="THB" name="total_beli" id="total_beli{{ $transaksi->id }}" value="{{(isset($transaksi->total_beli))?$transaksi->total_beli:''}} " type="text">
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Edit</button>
                      </div>
                    </form>
                    </div>
                  </div>
                </div>
                <!-- End Modal -->
                @section('js')

                <!-- untuk ajax yang ambil data -->
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

                <script type="text/javascript">
                  var total = 0;
                  var total_beli = 0;
                  var jumlah = document.getElementById("jumlah{{ $transaksi->id }}");
                  var harga = document.getElementById("harga{{ $transaksi->id }}");
                  var harga_beli = document.getElementById("harga_beli{{ $transaksi->id }}");
                  var total = document.getElementById("total{{ $transaksi->id }}");
                  var total_beli = document.getElementById("total_beli{{ $transaksi->id }}");
                  $(jumlah).keyup(function(){
                    var jumlahBarang = $(jumlah).val();
                    console.log(jumlah);
                    var hargaBarang = $(harga).val();
                    var hargaBeliBarang = $(harga_beli).val();
                    var subtotal = jumlahBarang*hargaBarang;
                    var subtotal_beli = jumlahBarang*hargaBeliBarang;
                    $(total).val(subtotal);
                    $(total_beli).val(subtotal_beli);
                  });
                  $(total).val(subtotal);
                  $(total_beli).val(subtotal_beli);
                </script>

                @stop
                @endforeach
                </tbody>
              </table>
              </div>
              <!-- ./box-body -->
              <div class="box-footer" align="center">
                
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>    
    
@endsection
