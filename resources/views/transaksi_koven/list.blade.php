@section('js')
  <script>$("#myModalError").modal("show");</script>
  <script>$("#myError").modal("show");</script>

@stop
@extends('layout_pos')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daftar Transaksi
        <small>Gamapay</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-pencil"></i> Dartar Transaksi</a></li>
        <li class="active">Gamapay</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div>
          @if(Session::has('success'))
            <div id="app" class="alert alert-success">
              {{ Session::get('success') }}
            </div>
          @elseif(Session::has('error'))
            <div class="alert alert-error">
              {{ Session::get('error') }}
            </div>
          @endif
        </div>
        @if (count($errors)>0)
          <div id="myError" class="modal fade" role="dialog">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-body text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <i class="fa fa-4x fa-exclamation-triangle" style="color:red"></i>
                          <h4 style="color:red">Ooops...</h4>
                          @foreach($errors->all() as $error)
                            <a>{{$error}}<br></a>
                          @endforeach
                      </div>
                  </div>
              </div>
          </div>
        @endif

      <div class="exportexcel"> 

      <form action="{{ url('transaksi_konven/export') }}" method="get" class="form-inline" enctype="multipart/form-data">
        <input type="hidden" value="{{\Carbon\Carbon::parse($requested->tgl_awal)->format('Y-m-d')}}" name="tgl_awal">
        <input type="hidden" value="{{\Carbon\Carbon::parse($requested->tgl_akhir)->format('Y-m-d')}}" name="tgl_akhir">
        <button type="submit" class="fa fa-file-excel-o btn btn-success">Export Excel</button>            
      </form>
      </div><br>
        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Daftar Transaksi</h3>
                <div class="exportexcel pull-right"> 
                  <form action="{{ url('transaksi_konven/list') }}" method="get" class="form-inline" enctype="multipart/form-data">
                    <div class="form-group">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                        </div>
                          <input placeholder="Masukan tanggal awal" class="form-control datepicker" name="tgl_awal" value="{{\Carbon\Carbon::parse($requested->tgl_awal)->format('Y-m-d')}}">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                        </div>
                          <input placeholder="Masukan tanggal akhir" class="form-control datepicker" name="tgl_akhir" value="{{\Carbon\Carbon::parse($requested->tgl_akhir)->format('Y-m-d')}}">
                      </div>
                    </div>
                    <button type="submit" class="btn btn-success">Cari</button>
                  </form>
                </div>
              </div>
              @if (session('error'))
                <div id="myError" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-body text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <i class="fa fa-4x fa-exclamation-triangle" style="color:red"></i>
                          <h4 style="color:red">Ooops...</h4>
                          <a>{{ session('error') }}</a>
                      </div>
                    </div>
                  </div>
                </div>
              @endif
              <!-- /.box-header -->
              <div class="box-body">                  
                <table id="example2" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Pembeli</th>
                    <th>Jenis Pembayaran</th>
                    <th>Status</th>
                    <th>Total Diskon</th>
                    <th>Total</th>
                    <th>Total Harga Beli</th>
                    <th>Tanggal</th>
                    <th width="10%">Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($list as $value => $transaksi)
                  <tr>
                    <td align="center">{{$value+1}}</td>
                    <td>{{$transaksi->nama}}</td>
                    <td>{{$transaksi->jenis_pembayaran}}</td>
                    <td>{{$transaksi->status}}</td>
                    <td>{{$transaksi->diskon}}</td>
                    <td>Rp {{number_format($transaksi->total,0,".",".")}},-</td>
                    <td>Rp {{number_format($transaksi->total_beli,0,".",".")}},-</td>
                    <td>{{\Carbon\Carbon::parse($transaksi->updated_at)->formatLocalized('%A, %d %B %Y')}}</td>
                    <td>
                      <a href="{{url('transaksi_konven/detail_transaksi_konven/'.$transaksi->id.'')}}" class="btn btn-primary btn-icon rounded-circle mg-r-5 mg-b-10" title="Lihat Detail">
                        Detail
                      </a>
                      <a data-toggle="modal" data-target="#myModal{{ $transaksi->id }}" class="btn btn-warning">Edit</a>
                    </td>
                    <!-- Modal -->
                    <div class="modal fade" id="myModal{{ $transaksi->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                        <!-- {!! Form::open(['url'=>'transaksi_konven/update_pos/'.$transaksi->id.'', 'role'=>'form'])!!} -->
                        <form action="{{url('transaksi_konven/update_pos/'.$transaksi->id.'')}}" method="post" enctype="multipart/form-data">
                          {{csrf_field()}}
                          <div class="modal-header">
                            <a class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></a>
                            <center><h2 class="modal-title" id="myModalLabel"><i class="fa fa-exclamation-circle"></i> Edit Data</h2></center>
                          </div>                  
                          <div class="modal-body">
                            <div class="form-group">
                              <label for="id">Id<span class="required" style="color:red">*</span></label>
                              <input class="form-control" placeholder="Id" name="id" id="id" value="{{(isset($transaksi->id))?$transaksi->id:''}} " type="text" readonly>
                            </div>        
                            <div class="form-group">
                              <label for="status">Status<span class="required" style="color:red">*</span></label>
                              <select class="form-control" name="status">
                                <option value="Lunas"  @if ($transaksi->status=='Lunas') selected="selected" @endif>Lunas</option>
                                <option value="Belum Bayar" @if ($transaksi->status=='Belum Bayar') selected="selected" @endif>Belum Bayar</option>
                              </select>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Edit</button>
                          </div>
                        <!-- {!!Form::close()!!} -->
                        </form>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- ./box-body -->
              <div class="box-footer" align="center">
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>    
    
@endsection
