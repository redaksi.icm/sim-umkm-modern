@extends('layout_pos')

@section('content')

<section class="content-header">
  <h1>
    Tambah Produk
    <small>Gamapay</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-pencil"></i> Daftar Produk</a></li>
    <li class="active"> Tambah Produk</li>
  </ol>
</section>

<section class="content">
  <div id="app">
    @if(Session::has('success'))
    <div class="alert alert-success">
      {{ Session::get('success') }}
    </div>
    @elseif(Session::has('error'))
    <div class="alert alert-error">
      {{ Session::get('error') }}
    </div>
    @endif
  </div>
  <div class="exportexcel">
  <a class="fa fa-download btn btn-warning pull-right" href="{{ url('barang_konven/downloadTemplateExcelPenjual') }}">Download Template Import Produk</a>
  <form  action="{{ url('barang_konven/import') }}" class="form-horizontal pull-left" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    @if ($errors->has('import'))
    <span class="invalid-feedback" role="alert" style="color: red">
        <strong>{{ $errors->first('import') }}</strong>
    </span>
    @endif
    <input type="file" name="import" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"/>
    <button class="btn btn-success pull-left">Import File</button>
  </form>
  </div><br><br><br>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Produk</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          {!! Form::open(array('url'=>'barang_konven/store_pos','files'=> 'true', 'novalidate',  'enctype'=>'multipart/form-data', 'method'=>'POST'))!!}
          <div class="form-group">
            <div class="col-md-8">
            <label for="kategori">Kategori<span class="required" style="color:red">*</span></label>
            <select class="form-control" name="kategori">
              <option value="">Pilih Kategeori</option>
              @foreach($kategori as $value => $key)
                <option value="{{$key->id}}" {{collect(old('kategori'))->contains($key->id) ? 'selected':''}}>{{$key->kategori}}</option>
              @endforeach
              </select>
              @if ($errors->has('kategori'))
              <span class="invalid-feedback" role="alert" style="color: red">
                <strong>{{ $errors->first('kategori') }}</strong>
              </span>
              @endif
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-3">
              <label for="nama">NB :</label>
              <p>Form Bertanda <span class="required" style="color:red">*</span> Wajib Diisi.</p>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-8">
            <label for="nama">Nama Barang<span class="required" style="color:red">*</span></label>
            <input class="form-control" placeholder="Nama Barang" name="nama" value="{{ old('nama') }}" type="text">
              @if ($errors->has('nama'))
              <span class="invalid-feedback" role="alert" style="color: red">
                <strong>{{ $errors->first('nama') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-8">
            <label for="deskripsi">Deskripsi</label>
              <textarea class="form-control" placeholder="Deskripsi" name="deskripsi" value="{{ old('deskripsi') }}" type="text"></textarea>
              @if ($errors->has('deskripsi'))
              <span class="invalid-feedback" role="alert" style="color: red">
                <strong>{{ $errors->first('deskripsi') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-8">
            <label for="harga">Harga<span class="required" style="color:red">*</span></label>
            <input class="form-control" placeholder="Harga" name="harga" value="{{ old('harga') }}" type="number">
              @if ($errors->has('harga'))
              <span class="invalid-feedback" role="alert" style="color: red">
                <strong>{{ $errors->first('harga') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-8">
            <label for="harga">Harga Beli<span class="required" style="color:red">*</span></label>
            <input class="form-control" placeholder="Harga" name="harga_beli" value="{{ old('harga_beli') }}" type="number">
              @if ($errors->has('harga_beli'))
              <span class="invalid-feedback" role="alert" style="color: red">
                <strong>{{ $errors->first('harga_beli') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-8">
              <label for="stok">Status<span class="required" style="color:red">*</span></label>
              <select class="form-control" name="stok" class="form-control col-md-7 col-xs-12">
                <option value="">Pilih Status</option>
                <option value="Tersedia">Tersedia</option>
                <option value="Habis">Habis</option>
              </select>
              @if ($errors->has('stok'))
              <span class="invalid-feedback" role="alert" style="color: red">
                <strong>{{ $errors->first('stok') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-8">
            <label for="diskon">Diskon</label>
            <input class="form-control" placeholder="Diskon (dalam rupiah)" name="diskon" value="{{ old('diskon') }}" type="number">
              @if ($errors->has('diskon'))
              <span class="invalid-feedback" role="alert" style="color: red">
                <strong>{{ $errors->first('diskon') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-8">
            <label for="gambar">Gambar</label>
              <input type="file" class="form-control" name="foto" placeholder="" value="{{old('foto')}}">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-8"><br>
              <button type="submit" class="btn btn-success">Simpan</button>
              <button type="button" class="btn btn-danger" onclick="location.href='{{url('barang_konven/list_pos')}}'">Batal</button>
            </div>
          </div>
          {!!Form::close()!!}
        </div>
        <!-- ./box-body -->
        <div class="box-footer" align="center">
        </div>
        <!-- /.box-footer -->
      </div>
    <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>

@endsection

@section('javascript')
<script src="{{asset('lib/highlightjs/highlight.pack.js')}}"></script>
<script src="{{asset('lib/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('lib/datatables-responsive/dataTables.responsive.js')}}"></script>
<script src="{{asset('lib/select2/js/select2.min.js')}}"></script>

<script>
    $(function(){
        'use strict';
        $('#datatable1').DataTable({
          responsive: false,
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });
        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
    });
</script>
@endsection
