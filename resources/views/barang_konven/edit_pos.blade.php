@section('js')
@stop
@extends('layout_pos')

@section('content')
  <section class="content-header">
    <h1>
      Edit Produk
      <small>Gamapay</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-pencil"></i> Daftar Produk</a></li>
      <li class="active"> Edit Produk</li>
    </ol>
  </section>
  <section class="content">
    <div><br>
      @if(Session::has('success'))
        <div id="app" class="alert alert-success">
          {{ Session::get('success') }}
        </div>
      @elseif(Session::has('error'))
        <div class="alert alert-error">
          {{ Session::get('error') }}
        </div>
      @endif
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Produk</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          {!! Form::open(array('url'=>'barang_konven/edit_pos/'.$data->id.'', 'method'=>'POST','files'=>'true'))!!}
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <img class="img-responsive avatar-view" src="{{asset('images/'.$data->foto.'')}}" alt="Avatar" title="Change Photos">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-8">
              <label for="gambar">Gambar</label>
                <input type="file" class="form-control" name="foto" placeholder="" value="{{old('foto')}}">
              </div>
              <div class="col-md-1">    
              </div>
              <div class="col-md-3">
                <label for="nama">NB :</label>
                <p>Form Bertanda <span class="required" style="color:red">*</span> Wajib Diisi.</p>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-8">
              <label for="kategori">Kategori<span class="required" style="color:red">*</span></label>
              @if (isset($data->kategori_id))
              <select class="form-control" name="kategori">
                <option value="1" @if ($data->kategori_id=='1') selected="selected" @endif>Makanan</option>
                <option value="2" @if ($data->kategori_id=='2') selected="selected" @endif>Minuman</option>
              </select>
              @endif
              @if ($errors->has('kategori'))
              <span class="invalid-feedback" role="alert" style="color: red">
                <strong>{{ $errors->first('kategori') }}</strong>
              </span>
              @endif
              </div>
            </div>
              <div class="form-group">
              <div class="col-md-8">
              <label for="nama">Nama Barang<span class="required" style="color:red">*</span></label>
              <input class="form-control" placeholder="Nama Barang" name="nama" value="{{(isset($data->nama))?$data->nama:''}} " type="text">
                @if ($errors->has('nama'))
                <span class="invalid-feedback" role="alert" style="color: red">
                  <strong>{{ $errors->first('nama') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-8">
              <label for="deskripsi">Deskripsi</label>
                <input class="form-control" cols="1" rows="2" placeholder="Deskripsi" name="deskripsi" value="{{(isset($data->deskripsi))?$data->deskripsi:''}} " type="text">
                @if ($errors->has('deskripsi'))
                <span class="invalid-feedback" role="alert" style="color: red">
                  <strong>{{ $errors->first('deskripsi') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-8">
              <label for="harga">Harga<span class="required" style="color:red">*</span></label>
              <input class="form-control" placeholder="Harga" name="harga" value="{{(isset($data->harga))?$data->harga:''}} " type="text">
                @if ($errors->has('harga'))
                <span class="invalid-feedback" role="alert" style="color: red">
                  <strong>{{ $errors->first('harga') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-8">
              <label for="harga">Harga Beli<span class="required" style="color:red">*</span></label>
              <input class="form-control" placeholder="Harga beli" name="harga_beli" value="{{(isset($data->harga_beli))?$data->harga_beli:''}} " type="text">
                @if ($errors->has('harga_beli'))
                <span class="invalid-feedback" role="alert" style="color: red">
                  <strong>{{ $errors->first('harga_beli') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-8">
              <label for="stok">Status<span class="required" style="color:red">*</span></label>
              <select class="form-control" name="stok" class="form-control col-md-7 col-xs-12">
                <option value="Tersedia"  @if ($data->status=='Tersedia') selected="selected" @endif>Tersedia</option>
                <option value="Habis" @if ($data->status=='Habis') selected="selected" @endif>Habis</option>
              </select>
                @if ($errors->has('stok'))
                <span class="invalid-feedback" role="alert" style="color: red">
                  <strong>{{ $errors->first('stok') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-8">
              <label for="diskon">Diskon<span class="required" style="color:red">*</span></label>
              <input class="form-control" placeholder="Diskon" name="diskon" value="{{(isset($data->diskon))?$data->diskon:''}} " type="text">
                @if ($errors->has('diskon'))
                <span class="invalid-feedback" role="alert" style="color: red">
                  <strong>{{ $errors->first('diskon') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-8"><br>
                <button type="submit" class="btn btn-success">Simpan</button>
                <button type="button" class="btn btn-danger" onclick="location.href='{{url('barang_konven/list_pos')}}'">Batal</button>
              </div>
            </div>
            {!!Form::close()!!}
          </div>
          <!-- ./box-body -->
          <div class="box-footer" align="center">
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@endsection

@section('javascript')
    <script src="{{asset('lib/highlightjs/highlight.pack.js')}}"></script>
    <script src="{{asset('lib/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('lib/datatables-responsive/dataTables.responsive.js')}}"></script>
    <script src="{{asset('lib/select2/js/select2.min.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>
    <script type="text/javascript">$('#app').fadeTo(300,1).fadeOut(1000);</script>

@endsection
