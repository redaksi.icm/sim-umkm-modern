@extends('master')

@section('css')
  <link href="{{asset('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">

@endsection

@section('navigation')
  <a href="{{url('/')}}"><i class="fa fa-home"></i> Dashboard</a> /
  <a href="#">User</a> /
  <a href="{{url('/master/penjual')}}">Penjual</a> /
  <a href="{{url('/master/penjual/'.$data->id.'')}}">Detail</a> /
  <a href="{{url('#')}}">Ubah</a>
@stop

@section('title')
  <h3>Penjual</h3>
@stop

@section('content')
{!! Form::open(['url'=>'master/penjual_konven/edit/'.$data->id.'', 'class'=>'form-horizontal', 'role'=>'form', 'files'=>true])!!}
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Ubah Data</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            {{-- <form class="form-horizontal form-label-left" novalidate> --}}
              {{-- <span class="section">Data Penjual</span> --}}
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Email <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input readonly type="text" placeholder="email" name="email" value="{{old('email') ? old('email') : $data->user['email']}}"  class="form-control col-md-7 col-xs-12">
                  @if ($errors->has('email'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nama Toko <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input readonly type="text" class="form-control col-md-7 col-xs-12" placeholder="username" name="name" value="{{old('name') ? old('name') : $data->user['name']}}" >
                  @if ($errors->has('name'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('name') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nomer Telepon <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="number" placeholder="no telepon" name="no_telepon" value="{{old('no_telepon') ? old('no_telepon') : $data->user['no_telepon']}}"  class="form-control col-md-7 col-xs-12">
                  @if ($errors->has('no_telepon'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('no_telepon') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nomer Rekening <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input readonly class="form-control" placeholder="" name="no_rek" value="{{old('no_rek') ? old('no_rek') : $data->no_rek}}" type="number"  class="form-control col-md-7 col-xs-12">
                  @if ($errors->has('no_rek'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('no_rek') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tanggal Lahir <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="input-group date">
                      <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                      </div>
                      <input name="tanggal_lahir" type="text" class="form-control datepicker" placeholder="MM/DD/YYYY" value="{{old('tanggal_lahir') ? old('tanggal_lahir') : \Carbon\Carbon::parse($data->user['tanggal_lahir'])->format('m/d/Y')}}" >
                      @if ($errors->has('tanggal_lahir'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{$errors->first('tanggal_lahir')}}</strong>
                          </span>
                      @endif
                  </div>
                </div>
              </div>
            
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <a class="btn btn-primary" onclick="location.href='{{url('master/penjual_konven/'.$data->id.'')}}'">Kembali</a>
                  <button id="send" type="submit" class="btn btn-success">Simpan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
{!!Form::close()!!}

        <!-- row -->
@endsection

@section('javascript')
    <script src="{{asset('lib/highlightjs/highlight.pack.js')}}"></script>
    <script src="{{asset('lib/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('lib/datatables-responsive/dataTables.responsive.js')}}"></script>
    <script src="{{asset('lib/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>

    <script>
        $(function(){
            'use strict';

            $('#datatable1').DataTable({
//                scrollX: true,
                responsive: false,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

            // Datepicker
            $('.fc-datepicker').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                dateFormat: 'dd-mm-yy'
            });

        });
    </script>
    <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
        // format: "dd/mm/yyyy",
        endDate: "dd",
        autoclose: true,
        todayHighlight: true,
      });
     });
    </script>

@endsection
